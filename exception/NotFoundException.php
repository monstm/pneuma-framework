<?php

namespace Pneuma\Exception;

use Exception;

/**
 * Every NotFound exception MUST implement this interface.
 */
class NotFoundException extends Exception
{
}
