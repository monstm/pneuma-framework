<?php

namespace Pneuma\Exception;

use Exception;

/**
 * Every MethodNotAllowed exception MUST implement this interface.
 */
class MethodNotAllowedException extends Exception
{
}
