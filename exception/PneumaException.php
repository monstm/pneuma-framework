<?php

namespace Pneuma\Exception;

use Exception;

/**
 * Every Pneuma exception MUST implement this interface.
 */
class PneumaException extends Exception
{
}
