<?php

namespace Pneuma\Interface;

use Symfony\Component\Console\Input\InputInterface as InputInputInterface;

/**
 * Describes Input interface.
 */
interface InputInterface extends InputInputInterface
{
}
