<?php

namespace Pneuma\Interface;

/**
 * Describes Base Path interface.
 */
interface BasePathInterface
{
    /**
     * Retrieve application base path.
     *
     * @param string $path Relative path
     * @return string
     */
    public function baseApp(string $path = ''): string;

    /**
     * Retrieve view base path.
     *
     * @param string $path Relative path
     * @return string
     */
    public function baseView(string $path = ''): string;

    /**
     * Retrieve resource base path.
     *
     * @param string $path Relative path
     * @return string
     */
    public function baseResource(string $path = ''): string;
}
