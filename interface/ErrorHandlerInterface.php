<?php

namespace Pneuma\Interface;

/**
 * Describes ErrorHandler interface.
 */
interface ErrorHandlerInterface
{
    /**
     * Retrieve API Response.
     *
     * @return ResponseInterface
     */
    public function getApiResponse(): ResponseInterface;

    /**
     * Retrieve HTML Response.
     *
     * @return ResponseInterface
     */
    public function getHtmlResponse(): ResponseInterface;
}
