<?php

namespace Pneuma\Interface;

/**
 * Describes Environment interface.
 */
interface EnvironmentInterface
{
    /**
     * Retrieve all of environment variables.
     *
     * @return array<string,string>
     */
    public function environments(): array;

    /**
     * Retrieve string value of environment variables.
     *
     * @param string $name Environment name
     * @param string $default Default value
     * @return string
     */
    public function env(string $name, string $default = ''): string;

    /**
     * Retrieve boolean value of environment variables.
     *
     * @param string $name Environment name
     * @param bool $default Default value
     * @return bool
     */
    public function envBool(string $name, bool $default = false): bool;

    /**
     * Retrieve integer value of environment variables.
     *
     * @param string $name Environment name
     * @param int $default Default value
     * @return int
     */
    public function envInt(string $name, int $default = 0): int;

    /**
     * Retrieve float value of environment variables.
     *
     * @param string $name Environment name
     * @param float $default Default value
     * @return float
     */
    public function envFloat(string $name, float $default = 0): float;
}
