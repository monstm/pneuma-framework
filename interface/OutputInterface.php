<?php

namespace Pneuma\Interface;

use Symfony\Component\Console\Output\OutputInterface as OutputOutputInterface;

/**
 * Describes Output interface.
 */
interface OutputInterface extends OutputOutputInterface
{
    /**
     * Write output with info style.
     *
     * @param string $message The message
     * @param bool $newLine Whether to add a newline
     * @return static
     */
    public function info(string $message, bool $newLine = false): self;

    /**
     * Write output with comment style.
     *
     * @param string $message The message
     * @param bool $newLine Whether to add a newline
     * @return static
     */
    public function comment(string $message, bool $newLine = false): self;

    /**
     * Write output with question style.
     *
     * @param string $message The message
     * @param bool $newLine Whether to add a newline
     * @return static
     */
    public function question(string $message, bool $newLine = false): self;

    /**
     * Write output with error style.
     *
     * @param string $message The message
     * @param bool $newLine Whether to add a newline
     * @return static
     */
    public function error(string $message, bool $newLine = false): self;
}
