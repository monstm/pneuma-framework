<?php

namespace Pneuma\Interface;

/**
 * Describes CLI Extension interface.
 */
interface CliExtensionInterface
{
    /**
     * Retrieve cli base path.
     *
     * @param string $path Relative path
     * @return string
     */
    public function baseCli(string $path = ''): string;

    /**
     * Retrieve command base path.
     *
     * @param string $path Relative path
     * @return string
     */
    public function baseCommand(string $path = ''): string;
}
