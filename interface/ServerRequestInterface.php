<?php

namespace Pneuma\Interface;

use Psr\Http\Message\ServerRequestInterface as MessageServerRequestInterface;

/**
 * Describes ServerRequest interface.
 */
interface ServerRequestInterface extends MessageServerRequestInterface
{
}
