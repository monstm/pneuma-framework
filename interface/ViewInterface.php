<?php

namespace Pneuma\Interface;

/**
 * Describes View interface.
 */
interface ViewInterface
{
    /**
     * Retrieve rendered view template.
     *
     * @param string $name View name
     * @param array<string,mixed> $data View data
     * @return string
     */
    public function view(string $name, array $data = []): string;
}
