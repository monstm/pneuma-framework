<?php

namespace Pneuma\Interface;

/**
 * Describes GCI Extension interface.
 */
interface CgiExtensionInterface
{
    /**
     * Retrieve base url.
     *
     * @param string $url Relative url
     * @return string
     */
    public function baseUrl(string $url = ''): string;

    /**
     * Retrieve cgi base path.
     *
     * @param string $path Relative path
     * @return string
     */
    public function baseCgi(string $path = ''): string;

    /**
     * Retrieve route base path.
     *
     * @param string $path Relative path
     * @return string
     */
    public function baseRoute(string $path = ''): string;
}
