<?php

namespace Pneuma\Interface;

use Pneuma\Exception\PneumaException;

/**
 * Describes Factory interface.
 */
interface FactoryInterface
{
    /**
     * Return an instance with provided base application path.
     *
     * @param string $path The application path.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withBaseApp(string $path): self;

    /**
     * Return an instance with provided base view path.
     *
     * @param string $path The base path.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withBaseView(string $path): self;

    /**
     * Return an instance with provided base resource path.
     *
     * @param string $path The base path.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withBaseResource(string $path): self;

    /**
     * Return an instance with provided SPL Autoload.
     *
     * @param string $namespace The namespace.
     * @param string $path The path.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withSplAutoload(string $namespace, string $path): self;

    /**
     * Return an instance with provided environment filename.
     *
     * @param string $filename The environment filename.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withEnvironment(string $filename): self;

    /**
     * Run application.
     *
     * @throws PneumaException If error.
     * @return void
     */
    public function run(): void;
}
