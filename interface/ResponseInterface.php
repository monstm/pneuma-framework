<?php

namespace Pneuma\Interface;

use Psr\Http\Message\ResponseInterface as MessageResponseInterface;
use SimpleXMLElement;

/**
 * Describes Response interface.
 */
interface ResponseInterface extends MessageResponseInterface
{
    /**
     * Return an instance with provided text/plain response.
     *
     * @param string $text Response content
     * @param int $status Response status
     * @return static
     */
    public function plain(string $text, int $status = 200): self;

    /**
     * Return an instance with provided text/html response.
     *
     * @param string $markUp Response content
     * @param int $status Response status
     * @return static
     */
    public function html(string $markUp, int $status = 200): self;

    /**
     * Return an instance with provided application/json response.
     *
     * @param array<mixed> $data Response content
     * @param bool $prettyPrint Response pretty print
     * @param int $status Response status
     * @return static
     */
    public function json(array $data, bool $prettyPrint = false, int $status = 200): self;

    /**
     * Return an instance with provided text/xml response.
     *
     * @param SimpleXMLElement $xml Response content
     * @param int $status Response status
     * @return static
     */
    public function xml(SimpleXMLElement $xml, int $status = 200): self;

    /**
     * Return an instance with provided inline disposition response.
     *
     * @param string $path File path
     * @param string $filename Filename
     * @return static
     */
    public function fileInline(string $path, string $filename = ''): self;

    /**
     * Return an instance with provided attachment disposition response.
     *
     * @param string $path File path
     * @param string $filename Filename
     * @return static
     */
    public function fileAttachment(string $path, string $filename = ''): self;

    /**
     * Return an instance with provided redirect response.
     *
     * @param string $url Location redirect
     * @param bool $permanent Permanent redirect
     * @return static
     */
    public function redirect(string $url, bool $permanent = false): self;
}
