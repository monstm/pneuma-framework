<?php

namespace Pneuma\Interface;

use Pneuma\Exception\PneumaException;

/**
 * Describes CGI interface.
 */
interface CgiInterface
{
    /**
     * Return an instance with provided base url.
     *
     * @param string $url The base url.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withBaseUrl(string $url): self;

    /**
     * Return an instance with provided base cgi path.
     *
     * @param string $path The base path.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withBaseCgi(string $path): self;

    /**
     * Return an instance with provided base route path.
     *
     * @param string $path The base path.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withBaseRoute(string $path): self;

    /**
     * Return an instance with provided error handler.
     *
     * @param callable $errorHandler The error handler.
     * @return static
     */
    public function withErrorHandler(callable $errorHandler): self;
}
