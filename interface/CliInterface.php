<?php

namespace Pneuma\Interface;

use Pneuma\Exception\PneumaException;

/**
 * Describes CLI interface.
 */
interface CliInterface
{
    /**
     * Return an instance with provided base cli path.
     *
     * @param string $path The base path.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withBaseCli(string $path): self;

    /**
     * Return an instance with provided base command path.
     *
     * @param string $path The base path.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withBaseCommand(string $path): self;
}
