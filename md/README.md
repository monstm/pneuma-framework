# Pneuma Framework

[
	![](https://badgen.net/packagist/v/pneuma/framework/latest)
	![](https://badgen.net/packagist/license/pneuma/framework)
	![](https://badgen.net/packagist/dt/pneuma/framework)
	![](https://badgen.net/packagist/favers/pneuma/framework)
](https://packagist.org/packages/pneuma/framework)

Pneuma is a PHP micro-framework that helps you quickly write simple yet powerful Common Gateway Interface and Command-Line Interface.

---

## Instalation

### Composer

Composer is a tool for dependency management in PHP.
It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

```sh
composer require pneuma/framework
```

Please see [Composer](https://getcomposer.org/) for more information.

---

## Support

* Repository: <https://gitlab.com/monstm/pneuma-framework>
* Documentations: <https://monstm.gitlab.io/pneuma-framework/>
* Annotation: <https://monstm.alwaysdata.net/pneuma-framework/>
* Issues: <https://gitlab.com/monstm/pneuma-framework/-/issues>
