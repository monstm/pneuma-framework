# Base

---

## Base Interface

Describes Base interface.

### baseApp

Retrieve application base path.

```php
$app_path = $pattern->baseApp($path = "");
```

### baseControl

Retrieve control base path.

```php
$control_path = $pattern->baseControl($path = "");
```

### baseModel

Retrieve model base path.

```php
$model_path = $pattern->baseModel($path = "");
```

### baseView

Retrieve view base path.

```php
$view_path = $pattern->baseView($path = "");
```

### baseLibrary

Retrieve library base path.

```php
$library_path = $pattern->baseLibrary($path = "");
```

### baseResource

Retrieve resource base path.

```php
$resource_path = $pattern->baseResource($path = "");
```
