# CLI

Perform an application in Command-Line Interface.
It lets you enables script using the system command line as runtime.

---

## CLI Instance

Implementation CLI instance.

```php
$factory = new \Pneuma\Cli();
```

---

## CLI Interface

Describes CLI interface.

### withBaseCli

Return an instance with provided base cli path.
Throw PneumaException if invalid.

```php
$factory = $factory->withBaseCli($cli_path);
```

### withBaseCommand

Return an instance with provided base command path.
Throw PneumaException if invalid.

```php
$factory = $factory->withBaseCommand($command_path);
```
