# CGI

Perform an application in Common Gateway Interface.
It lets you enables script using web servers as runtime.

---

## CGI Instance

Implementation CGI instance.

```php
$factory = new \Pneuma\Cgi();
```

---

## CGI Interface

Describes CGI interface.

### withBaseUrl

Return an instance with provided base url.
Throw PneumaException if invalid.

```php
$factory = $factory->withBaseUrl($base_url);
```

### withBaseCgi

Return an instance with provided base cgi path.
Throw PneumaException if invalid.

```php
$factory = $factory->withBaseCgi($cgi_path);
```

### withBaseRoute

Return an instance with provided base route path.
Throw PneumaException if invalid.

```php
$factory = $factory->withBaseRoute($route_path);
```

### withErrorDisplay

Return an instance with provided error display.

```php
$factory = $factory->withErrorDisplay($display);
```

### withErrorCallback

Return an instance with provided error callback.

```php
$callback = function(
    ServerRequestInterface $Request,
    ResponseInterface $Response,
    Exception $Exception
): ResponseInterface {
    return $Response
        ->withStatus(500)
        ->withHeader("Content-Type", "text/html")
        ->write("Something went wrong!");
};

$factory = $factory->withErrorCallback($callback);
```

### addMiddleware

Return an instance with added middleware scenarios.

```php
$factory = $factory->addMiddleware($middleware);
```
