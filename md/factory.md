# Pneuma Application Factory

Pneuma Application provides fast access to your applications.
It lets you quickly set up and make your applications active.
With Pneuma Application, you can easily access specifics interface.

---

## Factory Interface

Describes Factory interface.

### withBaseApp

Return an instance with provided base application path.
Throw PneumaException if invalid.

```php
$factory = $factory->withBaseApp($app_path);
```

### withEnvironment

Return an instance with provided environment filename.
Throw PneumaException if invalid.

```php
$factory = $factory->withEnvironment($filename);
```

### run

Run application.
Throw PneumaException if error.

```php
$factory->run();
```
