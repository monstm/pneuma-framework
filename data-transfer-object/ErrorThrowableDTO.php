<?php

namespace Pneuma\DataTransferObject;

use Throwable;

/**
 *  ErrorThrowable Data Transfer Object
 */
class ErrorThrowableDTO
{
    /** @var string */
    private $filename = '';

    /** @var int */
    private $line = 0;

    /** @var string */
    private $type = '';

    /** @var int */
    private $code = 0;

    /** @var string */
    private $message = '';

    /** @var array<ErrorTraceDTO> */
    private $traces = [];

    /**
     * @param Throwable $throwable
     */
    public function __construct(Throwable $throwable)
    {
        $this->filename = $throwable->getFile();
        $this->line = $throwable->getLine();
        $this->type = get_class($throwable);
        $this->code = $throwable->getCode();
        $this->message = $throwable->getMessage();

        $traces = [];
        foreach ($throwable->getTrace() as $trace) {
            array_push($traces, new ErrorTraceDTO($trace));
        }
        $this->traces = $traces;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @return int
     */
    public function getLine(): int
    {
        return $this->line;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return array<ErrorTraceDTO>
     */
    public function getTraces(): array
    {
        return $this->traces;
    }
}
