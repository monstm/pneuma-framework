<?php

namespace Pneuma\DataTransferObject;

use Pneuma\Exception\PneumaException;
use ReflectionClass;
use Samy\Validation\Validation;
use Samy\Validation\ValidationException;

/**
 *  Command Data Transfer Object
 */
class CommandDTO
{
    /** @var string */
    private $class = '';

    /** @var string */
    private $method = '';

    /** @var string */
    private $description = '';

    /** @var string */
    private $help = '';

    /** @var array<CommandArgumentDTO> */
    private $arguments = [];

    /** @var array<CommandOptionDTO> */
    private $options = [];

    /**
     * @param string $callback The callback.
     * @param array<string,mixed> $config The config data.
     * @throws ValidationException If invalid.
     */
    public function __construct(string $callback, array $config)
    {
        list($class, $method) = explode(':', $callback, 2);
        if (!class_exists($class)) {
            throw new PneumaException('Invalid Command Class: ' . $callback);
        }

        $reflection = new ReflectionClass($class);
        $instance = $reflection->newInstance();
        if (!method_exists($instance, $method)) {
            throw new PneumaException('Invalid Command Method: ' . $callback);
        }

        $validation = new Validation();
        $validation
            ->withRule('description', ['type' => 'string'])
            ->withRule('help', ['type' => 'string'])
            ->withRule('arguments', ['type' => 'array'])
            ->withRule('options', ['type' => 'array'])
            ->validate($config);

        $this->class = $class;
        $this->method = $method;
        /** @phpstan-ignore-next-line */
        $this->description = $config['description'] ?? '';
        /** @phpstan-ignore-next-line */
        $this->help = $config['help'] ?? '';

        $this->arguments = [];
        /** @phpstan-ignore-next-line */
        foreach ($config['arguments'] ?? [] as $argument) {
            array_push($this->arguments, new CommandArgumentDTO([
                /** @phpstan-ignore-next-line */
                'name' => $argument['name'] ?? '',
                /** @phpstan-ignore-next-line */
                'mode' => $argument['type'] ?? null,
                /** @phpstan-ignore-next-line */
                'description' => $argument['description'] ?? '',
                /** @phpstan-ignore-next-line */
                'default' => $argument['default'] ?? null,
            ]));
        }

        $this->options = [];
        /** @phpstan-ignore-next-line */
        foreach ($config['options'] ?? [] as $options) {
            array_push($this->options, new CommandOptionDTO([
                /** @phpstan-ignore-next-line */
                'name' => $options['name'] ?? '',
                /** @phpstan-ignore-next-line */
                'shortcut' => $options['alias'] ?? null,
                /** @phpstan-ignore-next-line */
                'mode' => $options['type'] ?? null,
                /** @phpstan-ignore-next-line */
                'description' => $options['description'] ?? '',
                /** @phpstan-ignore-next-line */
                'default' => $options['default'] ?? null,
            ]));
        }
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getHelp(): string
    {
        return $this->help;
    }

    /**
     * @return array<CommandArgumentDTO>
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }

    /**
     * @return array<CommandOptionDTO>
     */
    public function getOptions(): array
    {
        return $this->options;
    }
}
