<?php

namespace Pneuma\DataTransferObject;

use Samy\Validation\Validation;
use Samy\Validation\ValidationException;

/**
 *  Command Option Data Transfer Object
 */
class CommandOptionDTO
{
    /** @var string */
    private $name = '';

    /** @var null|string|array<string> */
    private $shortcut = null;

    /** @var ?int */
    private $mode = null;

    /** @var string */
    private $description = '';

    /** @var mixed */
    private $default = null;

    /**
     * @param array<string,mixed> $config The config data.
     * @throws ValidationException If invalid.
     */
    public function __construct(array $config)
    {
        $validation = new Validation();
        $validation
            ->withRule('name', ['required' => true, 'type' => 'string'])
            ->withRule('shortcut', ['type' => 'null|string|array'])
            ->withRule('mode', ['type' => 'integer|null'])
            ->withRule('description', ['type' => 'string'])
            ->validate($config);

        /** @phpstan-ignore-next-line */
        $this->name = $config['name'];

        /** @phpstan-ignore-next-line */
        $this->shortcut = $config['shortcut'] ?? null;

        /** @phpstan-ignore-next-line */
        $this->mode = $config['mode'] ?? null;

        /** @phpstan-ignore-next-line */
        $this->description = $config['description'] ?? '';

        $this->default = $config['default'] ?? null;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return null|string|array<string>
     */
    public function getShortcut(): null|string|array
    {
        return $this->shortcut;
    }

    /**
     * @return ?int
     */
    public function getMode(): ?int
    {
        return $this->mode;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getDefault(): mixed
    {
        return $this->default;
    }
}
