<?php

namespace Pneuma\DataTransferObject;

/**
 *  ErrorTrace Data Transfer Object
 */
class ErrorTraceDTO
{
    /** @var string */
    private $filename = '';

    /** @var int */
    private $line = 0;

    /** @var string */
    private $type = '';

    /** @var string */
    private $classname = '';

    /** @var string */
    private $function = '';

    /** @var array<mixed> */
    private $arguments = [];

    /**
     * @param array<string,mixed> $config The config data.
     */
    public function __construct(array $config)
    {
        /** @phpstan-ignore-next-line */
        $this->filename = $config['file'] ?? '';

        /** @phpstan-ignore-next-line */
        $this->line = $config['line'] ?? 0;

        /** @phpstan-ignore-next-line */
        $this->type = $config['type'] ?? '';

        /** @phpstan-ignore-next-line */
        $this->classname = $config['class'] ?? '';

        /** @phpstan-ignore-next-line */
        $this->function = $config['function'] ?? '';

        /** @phpstan-ignore-next-line */
        $this->arguments = $config['args'] ?? [];
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @return int
     */
    public function getLine(): int
    {
        return $this->line;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getClassname(): string
    {
        return $this->classname;
    }

    /**
     * @return string
     */
    public function getFunction(): string
    {
        return $this->function;
    }

    /**
     * @return array<mixed>
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }
}
