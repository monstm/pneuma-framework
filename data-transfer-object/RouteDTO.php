<?php

namespace Pneuma\DataTransferObject;

/**
 *  Route Data Transfer Object
 */
class RouteDTO
{
    /** @var string|array<string> */
    private $method = '';

    /** @var string */
    private $path = '';

    /** @var mixed */
    private $handler = null;

    /**
     * @param string|array<string> $method
     * @param string $path.
     * @param mixed $handler
     */
    public function __construct(string|array $method, string $path, mixed $handler)
    {
        $this->method = $method;
        $this->path = $path;
        $this->handler = $handler;
    }

    /**
     * @return string|array<string>
     */
    public function getMethod(): string|array
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return mixed
     */
    public function getHandler(): mixed
    {
        return $this->handler;
    }
}
