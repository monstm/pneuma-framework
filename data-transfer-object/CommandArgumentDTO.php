<?php

namespace Pneuma\DataTransferObject;

use Samy\Validation\Validation;
use Samy\Validation\ValidationException;

/**
 *  Command Argument Data Transfer Object
 */
class CommandArgumentDTO
{
    /** @var string */
    private $name = '';

    /** @var ?int */
    private $mode = null;

    /** @var string */
    private $description = '';

    /** @var mixed */
    private $default = null;

    /**
     * @param array<string,mixed> $config The config data.
     * @throws ValidationException If invalid.
     */
    public function __construct(array $config)
    {
        $validation = new Validation();
        $validation
            ->withRule('name', ['required' => true, 'type' => 'string'])
            ->withRule('mode', ['type' => 'integer|null'])
            ->withRule('description', ['type' => 'string'])
            ->validate($config);

        /** @phpstan-ignore-next-line */
        $this->name = $config['name'];

        /** @phpstan-ignore-next-line */
        $this->mode = $config['mode'] ?? null;

        /** @phpstan-ignore-next-line */
        $this->description = $config['description'] ?? '';

        $this->default = $config['default'] ?? null;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return ?int
     */
    public function getMode(): ?int
    {
        return $this->mode;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getDefault(): mixed
    {
        return $this->default;
    }
}
