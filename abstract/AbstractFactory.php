<?php

namespace Pneuma\Abstract;

use Pneuma\Exception\PneumaException;
use Pneuma\Interface\FactoryInterface;

/**
 * This is a simple Factory implementation that other Factory can inherit from.
 */
abstract class AbstractFactory implements FactoryInterface
{
    /** @var string */
    protected $baseApp = '';

    /** @var string */
    protected $baseView = '';

    /** @var string */
    protected $baseResource = '';

    /** @var array<string,string> */
    protected $splAutoload = [];

    /** @var string */
    protected $environment = '';

    /**
     * @param array<string,mixed> $config The configuration.
     */
    public function __construct(array $config = [])
    {
        /** @phpstan-ignore-next-line */
        $this->baseApp = $config['base_app'] ?? '';

        /** @phpstan-ignore-next-line */
        $this->baseView = $config['base_view'] ?? '';

        /** @phpstan-ignore-next-line */
        $this->baseResource = $config['base_resource'] ?? '';

        /** @phpstan-ignore-next-line */
        $this->splAutoload = $config['spl_autoload'] ?? [];

        /** @phpstan-ignore-next-line */
        $this->environment = $config['environment'] ?? '';
    }

    /**
     * Run application.
     *
     * @param array<string,mixed> $config The configuration.
     * @throws PneumaException If error.
     * @return void
     */
    abstract public function run(array $config = []): void;

    /**
     * Return an instance with provided base application path.
     *
     * @param string $path The application path.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withBaseApp(string $path): self
    {
        if (!is_dir($path)) {
            throw new PneumaException('Invalid Application Path: ' . $path);
        }
        /** @phpstan-ignore-next-line */
        $this->baseApp = realpath($path);

        return $this;
    }

    /**
     * Return an instance with provided base view path.
     *
     * @param string $path The base path.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withBaseView(string $path): self
    {
        if (!is_dir($path)) {
            throw new PneumaException('Invalid View Path: ' . $path);
        }
        /** @phpstan-ignore-next-line */
        $this->baseView = realpath($path);

        return $this;
    }

    /**
     * Return an instance with provided base resource path.
     *
     * @param string $path The base path.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withBaseResource(string $path): self
    {
        if (!is_dir($path)) {
            throw new PneumaException('Invalid Resource Path: ' . $path);
        }
        /** @phpstan-ignore-next-line */
        $this->baseResource = realpath($path);

        return $this;
    }

    /**
     * Return an instance with provided SPL Autoload.
     *
     * @param string $namespace The namespace.
     * @param string $path The path.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withSplAutoload(string $namespace, string $path): self
    {
        $name = str_replace('/', '\\', $namespace);
        $name = trim($name, " \t\n\r\0\x0B\\");
        if (empty($name)) {
            throw new PneumaException('Invalid SPL Autoload Namespace: ' . $namespace);
        }

        if (!is_dir($path)) {
            throw new PneumaException('Invalid SPL Autoload Path: ' . $path);
        }

        /** @phpstan-ignore-next-line */
        $this->splAutoload[$name] = realpath($path);

        return $this;
    }

    /**
     * Return an instance with provided environment filename.
     *
     * @param string $filename The environment filename.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withEnvironment(string $filename): self
    {
        if (!is_file($filename)) {
            throw new PneumaException('Invalid Environment Filename: ' . $filename);
        }
        $this->environment = $filename;

        return $this;
    }
}
