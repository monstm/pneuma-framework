<?php

namespace Pneuma\Abstract;

use Pneuma\Interface\ViewInterface;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * This is a simple View implementation that other View can inherit from.
 */
abstract class AbstractView extends AbstractBasePath implements ViewInterface
{
    /**
     * Retrieve rendered view template.
     *
     * @param string $name View name
     * @param array<string,mixed> $data View data
     * @return string
     */
    public function view(string $name, array $data = []): string
    {
        $path = $this->baseView();
        $loader = new FilesystemLoader($path);
        $environment = new Environment($loader);

        return $environment->render($name, $data);
    }
}
