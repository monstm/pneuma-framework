<?php

namespace Pneuma\Abstract;

use Pneuma\Config;
use Pneuma\Interface\EnvironmentInterface;
use Samy\Environment\VirtualEnvironment;

/**
 * This is a simple Environment implementation that other Environment can inherit from.
 */
abstract class AbstractEnvironment implements EnvironmentInterface
{
    /** @var VirtualEnvironment */
    private $virtualEnvironment = null;

    /**
     * @param array<string,mixed> $config The configuration.
     */
    public function __construct(array $config = [])
    {
        /** @phpstan-ignore-next-line */
        $this->virtualEnvironment = $config['virtual-environment'] ?? new VirtualEnvironment();

        $environment = $config['environment'] ?? Config::get('environment', '');
        if (!empty($environment)) {
            /** @phpstan-ignore-next-line */
            $this->virtualEnvironment->loadIni($environment);
        }
    }

    /**
     * Retrieve all of environment variables.
     *
     * @return array<string,string>
     */
    public function environments(): array
    {
        return $this->virtualEnvironment->getEnvironments();
    }

    /**
     * Retrieve string value of environment variables.
     *
     * @param string $name Environment name
     * @param string $default Default value
     * @return string
     */
    public function env(string $name, string $default = ''): string
    {
        return $this->virtualEnvironment->getString($name, $default);
    }

    /**
     * Retrieve boolean value of environment variables.
     *
     * @param string $name Environment name
     * @param bool $default Default value
     * @return bool
     */
    public function envBool(string $name, bool $default = false): bool
    {
        return $this->virtualEnvironment->getBoolean($name, $default);
    }

    /**
     * Retrieve integer value of environment variables.
     *
     * @param string $name Environment name
     * @param int $default Default value
     * @return int
     */
    public function envInt(string $name, int $default = 0): int
    {
        return $this->virtualEnvironment->getInteger($name, $default);
    }

    /**
     * Retrieve float value of environment variables.
     *
     * @param string $name Environment name
     * @param float $default Default value
     * @return float
     */
    public function envFloat(string $name, float $default = 0): float
    {
        return $this->virtualEnvironment->getFloat($name, $default);
    }
}
