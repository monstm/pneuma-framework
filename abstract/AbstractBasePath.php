<?php

namespace Pneuma\Abstract;

use Pneuma\Interface\BasePathInterface;

/**
 * This is a simple BasePath implementation that other BasePath can inherit from.
 */
abstract class AbstractBasePath extends AbstractEnvironment implements BasePathInterface
{
    /** @var array<string,string> */
    private $basePath = [];

    /**
     * @param array<string,mixed> $config The configuration.
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        /** @phpstan-ignore-next-line */
        $this->basePath = $config['base-path'] ?? [];
    }

    /**
     * Retrieve base path.
     *
     * @param string $key The base name
     * @param string $path Relative path
     * @return string
     */
    protected function basePath(string $key, string $path = ''): string
    {
        $prefix = $this->basePath[$key] ?? getcwd();
        $suffix = ltrim($this->safeSeparator($path), DIRECTORY_SEPARATOR);

        return rtrim($prefix . DIRECTORY_SEPARATOR . $suffix, DIRECTORY_SEPARATOR);
    }

    /**
     * Retrieve path with safe directory separator.
     *
     * @param string $path
     * @return string
     */
    private function safeSeparator(string $path): string
    {
        $ret = str_replace('/', '\\', $path);
        $ret = str_replace('\\', '/', $ret);

        $buffer = '';
        while ($buffer != $ret) {
            $buffer = $ret;
            $ret = str_replace('//', '/', $buffer);
        }

        if (DIRECTORY_SEPARATOR != '/') {
            $ret = str_replace('/', DIRECTORY_SEPARATOR, $ret);
        }

        return $ret;
    }

    /**
     * Retrieve application base path.
     *
     * @param string $path Relative path
     * @return string
     */
    public function baseApp(string $path = ''): string
    {
        return $this->basePath('app', $path);
    }

    /**
     * Retrieve view base path.
     *
     * @param string $path Relative path
     * @return string
     */
    public function baseView(string $path = ''): string
    {
        return $this->basePath('view', $path);
    }

    /**
     * Retrieve resource base path.
     *
     * @param string $path Relative path
     * @return string
     */
    public function baseResource(string $path = ''): string
    {
        return $this->basePath('resource', $path);
    }
}
