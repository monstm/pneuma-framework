<?php

namespace Pneuma;

use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use Pneuma\Abstract\AbstractFactory;
use Pneuma\Exception\MethodNotAllowedException;
use Pneuma\Exception\NotFoundException;
use Pneuma\Exception\PneumaException;
use Pneuma\Interface\CgiInterface;
use Pneuma\Interface\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Server\MiddlewareInterface;
use ReflectionClass;
use Samy\Psr7\Uri;
use Throwable;

use function FastRoute\simpleDispatcher;

/**
 * Simple CGI implementation.
 */
class Cgi extends AbstractFactory implements CgiInterface
{
    /** @var UriInterface */
    private $uri = null;

    /** @var string */
    private $baseCgi = '';

    /** @var string */
    private $baseRoute = '';

    /** @var callable */
    private $errorHandler = null;

    /** @var array<MiddlewareInterface> */
    private $middlewares = [];

    /**
     * @param array<string,mixed> $config The configuration.
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        /** @phpstan-ignore-next-line */
        $this->uri = $config['uri'] ?? new Uri();

        /** @phpstan-ignore-next-line */
        $this->baseCgi = $config['base_cgi'] ?? '';

        /** @phpstan-ignore-next-line */
        $this->baseRoute = $config['base_route'] ?? '';

        /** @phpstan-ignore-next-line */
        $this->errorHandler = $config['errorHandler'] ?? $this->errorHandlerFactory();
    }

    /**
     * @return callable
     */
    private function errorHandlerFactory(): callable
    {
        return function (): ResponseInterface {
            $errorHandler = new ErrorHandler();

            return $errorHandler->getHtmlResponse();
        };
    }

    /**
     * Return an instance with provided base url.
     *
     * @param string $url The base url.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withBaseUrl(string $url): self
    {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new PneumaException('Invalid URL: ' . $url);
        }

        $parseUrl = parse_url($url);
        $this->uri = new Uri([
            'scheme' => $parseUrl['scheme'] ?? 'http',
            'user' => $parseUrl['user'] ?? '',
            'password' => $parseUrl['pass'] ?? null,
            'host' => $parseUrl['host'] ?? 'localhost',
            'port' => $parseUrl['port'] ?? null,
            'query' => $parseUrl['query'] ?? ''
        ]);

        return $this;
    }

    /**
     * Return an instance with provided base cgi path.
     *
     * @param string $path The base path.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withBaseCgi(string $path): self
    {
        if (!is_dir($path)) {
            throw new PneumaException('Invalid CGI Path: ' . $path);
        }
        /** @phpstan-ignore-next-line */
        $this->baseCgi = realpath($path);

        return $this;
    }

    /**
     * Return an instance with provided base route path.
     *
     * @param string $path The base path.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withBaseRoute(string $path): self
    {
        if (!is_dir($path)) {
            throw new PneumaException('Invalid Route Path: ' . $path);
        }
        /** @phpstan-ignore-next-line */
        $this->baseRoute = realpath($path);

        return $this;
    }

    /**
     * Return an instance with provided error handler.
     *
     * @param callable $errorHandler The error handler.
     * @return static
     */
    public function withErrorHandler(callable $errorHandler): self
    {
        $this->errorHandler = $errorHandler;

        return $this;
    }

    /**
     * Return an instance with added middleware scenarios.
     *
     * @param mixed $middleware The middleware scenarios.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function addMiddleware(mixed $middleware): self
    {
        if (!$middleware instanceof MiddlewareInterface) {
            throw new PneumaException('Invalid MiddlewareInterface');
        }
        array_push($this->middlewares, $middleware);

        return $this;
    }

    /**
     * Run application.
     *
     * @param array<string,mixed> $config The configuration.
     * @throws PneumaException If error.
     * @return void
     */
    public function run(array $config = []): void
    {
        if (@session_status() == PHP_SESSION_NONE) {
            @session_start();
        }

        Route::reset();
        Pneuma::splAutoload($this->splAutoload);
        Pneuma::autoInclude($this->baseRoute);

        Config::reset();
        Config::set('base_url', rtrim($this->uri->__toString(), '/'));
        Config::set('base_cgi', $this->baseCgi);
        Config::set('base_app', $this->baseApp);
        Config::set('base_view', $this->baseView);
        Config::set('base_resource', $this->baseResource);
        Config::set('base_route', $this->baseRoute);
        Config::set('environment', $this->environment);

        $requestMethod = $_SERVER['REQUEST_METHOD'];
        $requestUri = $_SERVER['REQUEST_URI'];
        if (false !== $pos = strpos($requestUri, '?')) {
            $requestUri = substr($requestUri, 0, $pos);
        }

        $dispatcher = $this->getDispatcher();
        $routeInfo = $dispatcher->dispatch($requestMethod, rawurldecode($requestUri));
        $routeResponse = $this->getRouteResponse($routeInfo, $requestMethod, $requestUri);
        $this->applyResponse($routeResponse);
    }

    /**
     * @return Dispatcher
     */
    private function getDispatcher(): Dispatcher
    {
        $basePath = rtrim($this->uri->getPath(), '/');
        return simpleDispatcher(function (RouteCollector $routeCollector) use ($basePath) {
            foreach (Route::data() as $route) {
                $path = $basePath . '/' . ltrim($route->getPath(), '/');
                $routeCollector->addRoute($route->getMethod(), $path, $route->getHandler());
            }
        });
    }

    /**
     * @param array<mixed> $routeInfo
     * @param string $requestMethod
     * @param string $requestUri
     * @return ResponseInterface
     */
    private function getRouteResponse(array $routeInfo, string $requestMethod, string $requestUri): ResponseInterface
    {
        $serverRequest = new ServerRequest();
        $response = new Response();

        try {
            switch ($routeInfo[0]) {
                case Dispatcher::NOT_FOUND:
                    throw new NotFoundException($requestUri . ' not found');
                case Dispatcher::METHOD_NOT_ALLOWED:
                    throw new MethodNotAllowedException($requestMethod . ' ' . $requestUri . ' not allowed');
                case Dispatcher::FOUND:
                    $handler = $routeInfo[1] ?? '';
                    $arguments = $routeInfo[2] ?? [];
                    $handlerClass = $handler;
                    $handlerMethod = '';

                    if (str_contains($handler, ':')) {
                        list($handlerClass, $handlerMethod) = explode(':', $handler, 2);
                    } elseif (str_contains($handler, '@')) {
                        list($handlerClass, $handlerMethod) = explode('@', $handler, 2);
                        $handlerClass = 'App\\Control\\' . $handlerClass;
                    }

                    $reflection = new ReflectionClass($handlerClass);
                    $instance = $reflection->newInstance();
                    $response = $instance->{$handlerMethod}($serverRequest, $response, $arguments);
                    break;
            }
        } catch (Throwable $throwable) {
            $errorHandler = $this->errorHandler;
            $response = $errorHandler($throwable, $serverRequest);
        }

        return $response;
    }

    /**
     * @param ResponseInterface $response
     * @return static
     */
    private function applyResponse(ResponseInterface $response): self
    {
        http_response_code($response->getStatusCode());

        foreach ($response->getHeaders() as $name => $value) {
            header($name . ': ' . implode(', ', $value));
        }

        echo ($response->getBody());

        return $this;
    }
}
