<?php

namespace Pneuma;

use Pneuma\DataTransferObject\RouteDTO;

/**
 * Simple Route implementation.
 */
class Route
{
    /** @var array<RouteDTO> */
    private static $data = [];

    /**
     * Reset data provider.
     *
     * @return void
     */
    public static function reset(): void
    {
        self::$data = [];
    }

    /**
     * Retrieve data provider.
     * @return array<RouteDTO>
     */
    public static function data(): array
    {
        return self::$data;
    }

    /**
     * Add data provider.
     *
     * @param string $method The http method.
     * @param string $path The route path.
     * @param string $callback The callback.
     * @return void
     */
    public static function add(string $method, string $path, string $callback): void
    {
        $route = new RouteDTO($method, $path, $callback);

        if (!in_array($route, self::$data)) {
            array_push(self::$data, $route);
        }
    }
}
