<?php

namespace Pneuma;

use Pneuma\Interface\ServerRequestInterface;
use Samy\Psr7\ServerRequest as Psr7ServerRequest;

/**
 * Simple ServerRequest Implementation.
 */
class ServerRequest extends Psr7ServerRequest implements ServerRequestInterface
{
}
