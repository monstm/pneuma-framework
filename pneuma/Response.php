<?php

namespace Pneuma;

use Pneuma\Interface\ResponseInterface;
use Samy\Psr7\Response as Psr7Response;
use SimpleXMLElement;

/**
 * Simple Response Implementation.
 */
class Response extends Psr7Response implements ResponseInterface
{
    /**
     * Return an instance with provided text/plain response.
     *
     * @param string $text Response content
     * @param int $status Response status
     * @return static
     */
    public function plain(string $text, int $status = 200): self
    {
        return $this->response($text, $status, [
            'Content-Type' => 'text/plain',
            'Content-Length' => strval(strlen($text))
        ]);
    }

    /**
     * Return an instance with provided text/html response.
     *
     * @param string $markUp Response content
     * @param int $status Response status
     * @return static
     */
    public function html(string $markUp, int $status = 200): self
    {
        return $this->response($markUp, $status, [
            'Content-Type' => 'text/html',
            'Content-Length' => strval(strlen($markUp))
        ]);
    }

    /**
     * Return an instance with provided application/json response.
     *
     * @param array<mixed> $data Response content
     * @param bool $prettyPrint Response pretty print
     * @param int $status Response status
     * @return static
     */
    public function json(array $data, bool $prettyPrint = false, int $status = 200): self
    {
        $json = json_encode($data, ($prettyPrint ? JSON_PRETTY_PRINT : 0));
        $content = is_string($json) ? $json : '';

        return $this->response($content, $status, [
            'Content-Type' => 'application/json',
            'Content-Length' => strval(strlen($content))
        ]);
    }

    /**
     * Return an instance with provided text/xml response.
     *
     * @param SimpleXMLElement $xml Response content
     * @param int $status Response status
     * @return static
     */
    public function xml(SimpleXMLElement $xml, int $status = 200): self
    {
        $asXml = $xml->asXML();
        $content = is_string($asXml) ? $asXml : '';

        return $this->response($content, $status, [
            'Content-Type' => 'text/xml',
            'Content-Length' => strval(strlen($content))
        ]);
    }

    /**
     * Return an instance with provided inline disposition response.
     *
     * @param string $path File path
     * @param string $filename Filename
     * @return static
     */
    public function fileInline(string $path, string $filename = ''): self
    {
        return $this->file('inline', $path, $filename);
    }

    /**
     * Return an instance with provided attachment disposition response.
     *
     * @param string $path File path
     * @param string $filename Filename
     * @return static
     */
    public function fileAttachment(string $path, string $filename = ''): self
    {
        return $this->file('attachment', $path, $filename);
    }

    /**
     * Return an instance with provided file response.
     *
     * @param string $disposition Disposition type
     * @param string $path File path
     * @param string $filename Filename
     * @return static
     */
    private function file(string $disposition, string $path, string $filename = ''): self
    {
        $content = '';
        $status = 404;
        $header = [];

        if (is_file($path)) {
            $fileGetContents = file_get_contents($path);
            $content = is_string($fileGetContents) ? $fileGetContents : '';
            $status = 200;
            $basename = $filename != '' ? $filename : pathinfo($path, PATHINFO_BASENAME);
            $header['Content-Disposition'] = $disposition . '; filename="' . $basename . '"';

            $mimeContentType = mime_content_type($path);
            if (is_string($mimeContentType)) {
                $header['Content-Type'] = $mimeContentType;
            }

            $filesize = filesize($path);
            if (is_int($filesize)) {
                $header['Content-Length'] = strval($filesize);
            }
        }

        return $this->response($content, $status, $header);
    }

    /**
     * Return an instance with provided redirect response.
     *
     * @param string $url Location redirect
     * @param bool $permanent Permanent redirect
     * @return static
     */
    public function redirect(string $url, bool $permanent = false): self
    {
        $content = '<!DOCTYPE html>' .
            '<html>' .
            '<head>' .
            '<meta http-equiv="refresh" content="0; url=' . $url . '" />' .
            '<script type="text/javascript">window.location.href = \'' . $url . '\';</script>' .
            '</head>' .
            '<body><a href="' . $url . '">' . $url . '</a></body>' .
            '</html>';

        return $this->response($content, ($permanent ? 301 : 302), [
            'Content-Type' => 'text/html',
            'Content-Length' => strval(strlen($content)),
            'Location' => $url
        ]);
    }


    /**
     * Generate PSR-7 response.
     *
     * @param string $body Response content
     * @param int $status Response status
     * @param array<string,string> $header Response header
     * @return static
     */
    private function response(string $body, int $status = 200, array $header = []): self
    {
        $this->withStatus($status);

        foreach ($header as $key => $value) {
            $this->withHeader($key, $value);
        }

        $this->getBody()->write($body);

        return $this;
    }
}
