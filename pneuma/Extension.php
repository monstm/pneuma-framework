<?php

namespace Pneuma;

use Pneuma\Abstract\AbstractView;

/**
 * This is a simple Extension implementation that other Extension can inherit from.
 */
abstract class Extension extends AbstractView
{
    /**
     * @param array<string,mixed> $config The configuration.
     */
    public function __construct(array $config = [])
    {
        $default = [
            'base-path' => [
                'app' => Config::get('base_app'),
                'view' => Config::get('base_view'),
                'resource' => Config::get('base_resource')
            ]
        ];

        $construct = array_replace_recursive($default, $config);
        parent::__construct($construct);
    }
}
