<?php

namespace Pneuma;

use Pneuma\Interface\InputInterface;
use Symfony\Component\Console\Input\ArgvInput;

/**
 * Simple ServerRequest Implementation.
 */
class Input extends ArgvInput implements InputInterface
{
}
