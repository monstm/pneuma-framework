<?php

namespace Pneuma;

use ReflectionClass;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Simple CLI Command implementation.
 */
class CliCommand extends Command
{
    /** @var string */
    private $class = '';

    /** @var string */
    private $method = '';

    /**
     * Return an instance with provided classname.
     *
     * @param string $class The classname.
     * @return static
     */
    public function setClass(string $class): self
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Return an instance with provided method callback.
     *
     * @param string $method The method callback.
     * @return static
     */
    public function setMethod(string $method): self
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @return int 0 if everything went fine, or an exit code
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $definition = $this->getDefinition();
        $cliInput = new Input($_SERVER['argv'], $definition);

        $verbosity = $output->getVerbosity();
        $formater = $output->getFormatter();
        $isDecorated = $formater->isDecorated();
        $cliOutput = new Output($verbosity, $isDecorated, $formater);

        /** @phpstan-ignore-next-line */
        $reflection = new ReflectionClass($this->class);
        $instance = $reflection->newInstance();
        $result = $instance->{$this->method}($cliInput, $cliOutput);

        if (!is_bool($result)) {
            return Command::INVALID;
        }

        return $result ? Command::SUCCESS : Command::FAILURE;
    }
}
