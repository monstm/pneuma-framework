<?php

namespace Pneuma;

use Pneuma\Abstract\AbstractFactory;
use Pneuma\Exception\PneumaException;
use Pneuma\Interface\CliInterface;
use Symfony\Component\Console\Application;

/**
 * Simple CLI implementation.
 */
class Cli extends AbstractFactory implements CliInterface
{
    /** @var string */
    private $baseCli = '';

    /** @var string */
    private $baseCommand = '';

    /**
     * @param array<string,mixed> $config The configuration.
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        /** @phpstan-ignore-next-line */
        $this->baseCli = $config['base_cli'] ?? '';

        /** @phpstan-ignore-next-line */
        $this->baseCommand = $config['base_command'] ?? '';
    }

    /**
     * Return an instance with provided base cli path.
     *
     * @param string $path The base path.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withBaseCli(string $path): self
    {
        if (!is_dir($path)) {
            throw new PneumaException('Invalid CLI Path: ' . $path);
        }
        /** @phpstan-ignore-next-line */
        $this->baseCli = realpath($path);

        return $this;
    }

    /**
     * Return an instance with provided base command path.
     *
     * @param string $path The base path.
     * @throws PneumaException If invalid.
     * @return static
     */
    public function withBaseCommand(string $path): self
    {
        if (!is_dir($path)) {
            throw new PneumaException('Invalid Command Path: ' . $path);
        }
        /** @phpstan-ignore-next-line */
        $this->baseCommand = realpath($path);

        return $this;
    }

    /**
     * Run application.
     *
     * @param array<string,mixed> $config The configuration.
     * @throws PneumaException If error.
     * @return void
     */
    public function run(array $config = []): void
    {
        Command::reset();
        Pneuma::splAutoload($this->splAutoload);
        Pneuma::autoInclude($this->baseCommand);

        Config::reset();
        Config::set('base_cli', $this->baseCli);
        Config::set('base_app', $this->baseApp);
        Config::set('base_view', $this->baseView);
        Config::set('base_resource', $this->baseResource);
        Config::set('base_command', $this->baseCommand);
        Config::set('environment', $this->environment);

        $application = $config['application'] ?? new Application(
            /** @phpstan-ignore-next-line */
            $config['name'] ?? 'UNKNOWN',
            /** @phpstan-ignore-next-line */
            $config['version'] ?? 'UNKNOWN'
        );

        foreach (Command::data() as $name => $command) {
            $cliCommand = new CliCommand();

            $cliCommand
                ->setName($name)
                ->setClass($command->getClass())
                ->setMethod($command->getMethod())
                ->setDescription($command->getDescription())
                ->setHelp($command->getHelp());

            foreach ($command->getArguments() as $argument) {
                $cliCommand->addArgument(
                    $argument->getName(),
                    $argument->getMode(),
                    $argument->getDescription(),
                    $argument->getDefault()
                );
            }

            foreach ($command->getOptions() as $option) {
                $cliCommand->addOption(
                    $option->getName(),
                    $option->getShortcut(),
                    $option->getMode(),
                    $option->getDescription(),
                    $option->getDefault()
                );
            }

            /** @phpstan-ignore-next-line */
            $application->add($cliCommand);
        }

        /** @phpstan-ignore-next-line */
        $application->run();
    }
}
