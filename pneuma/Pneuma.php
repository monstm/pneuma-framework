<?php

namespace Pneuma;

use Pneuma\Exception\PneumaException;

/**
 * Simple Pneuma implementation.
 */
class Pneuma
{
    /**
     * Standard PHP Library autoload.
     *
     * @param array<string,string> $data
     * @return void
     */
    public static function splAutoload(array $data): void
    {
        spl_autoload_register(function ($class) use ($data) {
            foreach ($data as $name => $directory) {
                $namespace = 'App\\' . $name . '\\';
                $strlen = strlen($namespace);
                if ($namespace != substr($class, 0, $strlen)) {
                    continue;
                }

                $filename = $directory . DIRECTORY_SEPARATOR . substr($class, $strlen) . '.php';
                if (DIRECTORY_SEPARATOR != '\\') {
                    $filename = str_replace('\\', DIRECTORY_SEPARATOR, $filename);
                }

                if (is_file($filename)) {
                    require_once($filename);
                    break;
                }
            }
        });
    }

    /**
     * Auto-Include PHP files.
     *
     * @param string $directory The directory.
     * @throws PneumaException If invalid.
     * @return void
     */
    public static function autoInclude(string $directory): void
    {
        if (!is_dir($directory)) {
            throw new PneumaException('Invalid directory: ' . $directory);
        }

        $scandirs = scandir($directory);
        if (!is_array($scandirs)) {
            throw new PneumaException('Failed scan directory: ' . $directory);
        }

        foreach ($scandirs as $scandir) {
            $filename = realpath($directory . DIRECTORY_SEPARATOR . $scandir);
            /** @phpstan-ignore-next-line */
            if (!is_file($filename)) {
                continue;
            }

            /** @phpstan-ignore-next-line */
            if (strtolower(pathinfo($filename, PATHINFO_EXTENSION)) == 'php') {
                include_once($filename);
            }
        }
    }
}
