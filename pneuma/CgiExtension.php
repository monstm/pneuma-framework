<?php

namespace Pneuma;

/**
 * This is a simple CGI Extension implementation that other CGI Extension can inherit from.
 */
abstract class CgiExtension extends Extension
{
    /** @var string */
    private $baseUrl = '';

    /**
     * @param array<string,mixed> $config The configuration.
     */
    public function __construct(array $config = [])
    {
        $default = [
            'base-path' => [
                'cgi' => Config::get('base_cgi'),
                'route' => Config::get('base_route')
            ]
        ];

        $construct = array_replace_recursive($default, $config);
        parent::__construct($construct);

        /** @phpstan-ignore-next-line */
        $this->baseUrl = $config['base-url'] ?? Config::get('base_url', 'http://localhost');
    }

    /**
     * Retrieve base url.
     *
     * @param string $url Relative url
     * @return string
     */
    public function baseUrl(string $url = ''): string
    {
        $prefix = $this->baseUrl;
        $suffix = ltrim($url, '/');

        return rtrim($prefix . '/' . $suffix, '/');
    }

    /**
     * Retrieve cgi base path.
     *
     * @param string $path Relative path
     * @return string
     */
    public function baseCgi(string $path = ''): string
    {
        return $this->basePath('cgi', $path);
    }

    /**
     * Retrieve route base path.
     *
     * @param string $path Relative path
     * @return string
     */
    public function baseRoute(string $path = ''): string
    {
        return $this->basePath('route', $path);
    }
}
