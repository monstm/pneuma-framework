<?php

namespace Pneuma;

use Pneuma\Interface\OutputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Simple Output Implementation.
 */
class Output extends ConsoleOutput implements OutputInterface
{
    /**
     * Write output with style.
     *
     * @param string $name Style name
     * @param string $message The message
     * @param bool $newLine Whether to add a newline
     * @return static
     */
    private function style(string $name, string $message, bool $newLine): self
    {
        $this->write('<' . $name . '>' . $message . '</' . $name . '>', $newLine);

        return $this;
    }

    /**
     * Write output with info style.
     *
     * @param string $message The message
     * @param bool $newLine Whether to add a newline
     * @return static
     */
    public function info(string $message, bool $newLine = false): self
    {
        return $this->style('info', $message, $newLine);
    }

    /**
     * Write output with comment style.
     *
     * @param string $message The message
     * @param bool $newLine Whether to add a newline
     * @return static
     */
    public function comment(string $message, bool $newLine = false): self
    {
        return $this->style('comment', $message, $newLine);
    }

    /**
     * Write output with question style.
     *
     * @param string $message The message
     * @param bool $newLine Whether to add a newline
     * @return static
     */
    public function question(string $message, bool $newLine = false): self
    {
        return $this->style('question', $message, $newLine);
    }

    /**
     * Write output with error style.
     *
     * @param string $message The message
     * @param bool $newLine Whether to add a newline
     * @return static
     */
    public function error(string $message, bool $newLine = false): self
    {
        return $this->style('error', $message, $newLine);
    }
}
