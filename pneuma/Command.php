<?php

namespace Pneuma;

use Pneuma\DataTransferObject\CommandDTO;

/**
 * Simple Command implementation.
 */
class Command
{
    /** @var array<string,CommandDTO> */
    private static $data = [];

    /**
     * Reset data provider.
     *
     * @return void
     */
    public static function reset(): void
    {
        self::$data = [];
    }

    /**
     * Retrieve data provider.
     * @return array<string,CommandDTO>
     */
    public static function data(): array
    {
        return self::$data;
    }

    /**
     * Add data provider.
     *
     * @param string $name The name.
     * @param string $callback The callback.
     * @param array<mixed> $config The config.
     * @return void
     */
    public static function add(string $name, string $callback, array $config): void
    {
        self::$data[$name] = new CommandDTO($callback, $config);
    }
}
