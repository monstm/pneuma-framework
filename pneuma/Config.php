<?php

namespace Pneuma;

/**
 * Simple Config implementation.
 */
class Config
{
    /** @var array<string,mixed> */
    private static $data = [];

    /**
     * Reset data provider.
     *
     * @return void
     */
    public static function reset(): void
    {
        self::$data = [];
    }

    /**
     * Set attribute.
     *
     * @param string $name The attribute name.
     * @param mixed $value The value of the attribute.
     * @return void
     */
    public static function set(string $name, $value): void
    {
        self::$data[$name] = $value;
    }

    /**
     * Retrieve an attribute.
     *
     * @param string $name The attribute name.
     * @param mixed $default Default value to return if the attribute does not exist.
     * @return mixed
     */
    public static function get(string $name, $default = null): mixed
    {
        return self::$data[$name] ?? $default;
    }
}
