<?php

namespace Pneuma;

use Pneuma\DataTransferObject\ErrorThrowableDTO;
use Pneuma\DataTransferObject\ErrorTraceDTO;
use Pneuma\Interface\ErrorHandlerInterface;
use Pneuma\Interface\ResponseInterface;
use Throwable;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * Simple CGI implementation.
 */
class ErrorHandler implements ErrorHandlerInterface
{
    /** @var int */
    private $status = 0;

    /** @var string */
    private $error = '';

    /** @var string */
    private $errorDescription = '';

    /** @var string */
    private $errorUri = '';

    /** @var ?ErrorThrowableDTO */
    private $throwable = null;

    /**
     * @param array<string,mixed> $config The configuration.
     */
    public function __construct(array $config = [])
    {
        /** @phpstan-ignore-next-line */
        $this->status = $config['status'] ?? 500;

        /** @phpstan-ignore-next-line */
        $this->error = $config['error'] ?? 'Internal Server Error';

        /** @phpstan-ignore-next-line */
        $this->errorDescription = $config['error_description'] ?? '';

        /** @phpstan-ignore-next-line */
        $this->errorUri = $config['error_uri'] ?? '';

        $throwable = $config['throwable'] ?? null;
        $this->throwable = $throwable instanceof Throwable ? new ErrorThrowableDTO($throwable) : null;
    }

    /**
     * Retrieve API Response.
     *
     * @return ResponseInterface
     */
    public function getApiResponse(): ResponseInterface
    {
        $response = new Response();
        $data = $this->getData();

        return $response->json($data, false, $this->status);
    }

    /**
     * Retrieve HTML Response.
     *
     * @return ResponseInterface
     */
    public function getHtmlResponse(): ResponseInterface
    {
        $response = new Response();
        $data = $this->getData();
        $traces = $data['traces'] ?? null;

        if (is_array($traces)) {
            $data['traces'] = array_map(
                function (array $trace) {
                    $arguments = [];

                    foreach ($trace['arguments'] as $argument) {
                        $type = strtolower(gettype($argument));

                        switch ($type) {
                            case 'boolean':
                                $value = $argument ? 'true' : 'false';
                                break;
                            case 'integer':
                            case 'double':
                                $value = strval($argument);
                                break;
                            case 'string':
                                $value = '"' . addslashes($argument) . '"';
                                break;
                            default:
                                $value = $type;
                                break;
                        }

                        array_push($arguments, $value);
                    }

                    $trace['_arguments'] = implode(', ', $arguments);

                    return $trace;
                },
                $traces
            );
        }

        $path = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'twig';
        $loader = new FilesystemLoader($path);
        $environment = new Environment($loader);
        $markup = $environment->render('error-handler.twig', $data);

        return $response->html($markup, $this->status);
    }

    /**
     * @return array<string,mixed>
     */
    private function getData(): array
    {
        $data = [
            'status' => $this->status,
            'success' => false,
            'error' => $this->error,
        ];

        if (!empty($this->errorDescription)) {
            $data['error_description'] = $this->errorDescription;
        }

        if (!empty($this->errorUri)) {
            $data['error_uri'] = $this->errorUri;
        }

        if (!empty($this->throwable)) {
            $data['detail'] = [
                'filename' => $this->throwable->getFilename(),
                'line' => $this->throwable->getLine(),
                'type' => $this->throwable->getType(),
                'code' => $this->throwable->getCode(),
                'message' => $this->throwable->getMessage(),
            ];

            $data['traces'] = array_map(
                function (ErrorTraceDTO $trace) {
                    return [
                        'filename' => $trace->getFilename(),
                        'line' => $trace->getLine(),
                        'type' => $trace->getType(),
                        'classname' => $trace->getClassname(),
                        'function' => $trace->getFunction(),
                        'arguments' => $trace->getArguments(),
                    ];
                },
                $this->throwable->getTraces()
            );
        }

        return $data;
    }
}
