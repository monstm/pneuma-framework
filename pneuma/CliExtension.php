<?php

namespace Pneuma;

/**
 * This is a simple CLI Extension implementation that other CLI Extension can inherit from.
 */
abstract class CliExtension extends Extension
{
    /**
     * @param array<string,mixed> $config The configuration.
     */
    public function __construct(array $config = [])
    {
        $default = [
            'base-path' => [
                'cli' => Config::get('base_cli'),
                'command' => Config::get('base_command')
            ]
        ];

        $construct = array_replace_recursive($default, $config);
        parent::__construct($construct);
    }

    /**
     * Retrieve cli base path.
     *
     * @param string $path Relative path
     * @return string
     */
    public function baseCli(string $path = ''): string
    {
        return $this->basePath('cli', $path);
    }

    /**
     * Retrieve command base path.
     *
     * @param string $path Relative path
     * @return string
     */
    public function baseCommand(string $path = ''): string
    {
        return $this->basePath('command', $path);
    }
}
