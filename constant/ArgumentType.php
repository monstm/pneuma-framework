<?php

namespace Pneuma\Constant;

use Symfony\Component\Console\Input\InputArgument;

/**
 * Simple Argument Type implementation.
 */
class ArgumentType
{
    public const MANDATORY  = InputArgument::REQUIRED; // behavior
    public const OPTIONAL   = InputArgument::OPTIONAL;
    public const ARRAY      = InputArgument::IS_ARRAY;
}
