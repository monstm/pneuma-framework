<?php

namespace Pneuma\Constant;

/**
 * Simple Table Default implementation.
 */
class TableDefault
{
    public const STYLE              = 'default';
    public const HEADER_TEXT        = '';
    public const COLUMN_WIDTH       = 0;
    public const COLUMN_MAX_WIDTH   = 0;
}
