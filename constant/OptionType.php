<?php

namespace Pneuma\Constant;

use Symfony\Component\Console\Input\InputOption;

/**
 * Simple Option Type implementation.
 */
class OptionType
{
    public const BLANK      = InputOption::VALUE_NONE;
    public const VALUE      = InputOption::VALUE_REQUIRED; // behavior
    public const OPTIONAL   = InputOption::VALUE_OPTIONAL;
    public const ARRAY      = InputOption::VALUE_IS_ARRAY;
    public const REVERSE    = InputOption::VALUE_NEGATABLE;
}
