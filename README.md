# Pneuma Framework

## Composer update

> docker run --rm -v .:/app composer up

## Running local test

* docker run --rm -v .:/pneuma -w /pneuma php:cli-alpine php vendor/bin/phpcs --standard=PSR1,PSR2,PSR12 constant data-transfer-object interface abstract pneuma
* docker run --rm -v .:/pneuma -w /pneuma php:cli-alpine php vendor/bin/phpstan analyse -c phpstan.neon
* docker run --rm -v .:/pneuma -w /pneuma php:cli-alpine php vendor/bin/phpunit
